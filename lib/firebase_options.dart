// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBs9c1WMUmkU84_0bncI_2hlZnyr9KHRes',
    appId: '1:416059328702:web:a0089eaef0eaa721f8b6ef',
    messagingSenderId: '416059328702',
    projectId: 'share-a55dc',
    authDomain: 'share-a55dc.firebaseapp.com',
    storageBucket: 'share-a55dc.appspot.com',
    measurementId: 'G-2Z08E79FT2',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBDV5NCRx2Pw2U8sIJ-FmAH0kbElrAiOh4',
    appId: '1:416059328702:android:3c2cb9fa9d251fdbf8b6ef',
    messagingSenderId: '416059328702',
    projectId: 'share-a55dc',
    storageBucket: 'share-a55dc.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyBs61mV55texJfQN8RXWiPSy3aIjOMxbUQ',
    appId: '1:416059328702:ios:74b4b834ded8887cf8b6ef',
    messagingSenderId: '416059328702',
    projectId: 'share-a55dc',
    storageBucket: 'share-a55dc.appspot.com',
    iosClientId: '416059328702-rpdq10fo22m1008rt17t4mc72q9rqvk9.apps.googleusercontent.com',
    iosBundleId: 'com.example.share',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyBs61mV55texJfQN8RXWiPSy3aIjOMxbUQ',
    appId: '1:416059328702:ios:74b4b834ded8887cf8b6ef',
    messagingSenderId: '416059328702',
    projectId: 'share-a55dc',
    storageBucket: 'share-a55dc.appspot.com',
    iosClientId: '416059328702-rpdq10fo22m1008rt17t4mc72q9rqvk9.apps.googleusercontent.com',
    iosBundleId: 'com.example.share',
  );
}
