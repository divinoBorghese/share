import 'dart:async';
import 'dart:math' show asin, atan2, cos, pi, sin, sqrt;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:geolocator/geolocator.dart';

import 'letturaqr.dart';

final FirebaseFirestore _db = FirebaseFirestore.instance;
final _userStream =
    FirebaseFirestore.instance.collection('passeggini').snapshots();

Color _disponibileColor(String s) {
  if (s == 'si')
    return Colors.redAccent;
  else
    return Colors.greenAccent;
}

class Home extends StatefulWidget {
    final User user;

  const Home({Key? key, required this.user}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Position? _currentPosition;
  StreamSubscription<Position>? _positionStreamSubscription;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  @override
  void dispose() {
    super.dispose();
    _positionStreamSubscription?.cancel();
  }

  Future<void> _getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Servizio di localizzazione disabilitato');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      // Controlla se il permesso è stato negato in modo permanente, in tal caso, mostra un messaggio e chiedi di andare alle impostazioni
      if (await Permission.location.isPermanentlyDenied) {
        return Future.error(
            'Autorizzazione alla localizzazione negata in modo permanente. Vai alle impostazioni dell\'app per abilitare la localizzazione.');
      } else {
        // Se il permesso non è stato negato in modo permanente, richiedi il permesso
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Autorizzazione alla localizzazione negata');
        }
      }
    }

    Position position = await Geolocator.getCurrentPosition();
    setState(() {
      _currentPosition = position;
    });

    // Aggiorna la posizione corrente quando cambia
    _positionStreamSubscription =
        Geolocator.getPositionStream().listen((Position newPosition) {
      setState(() {
        _currentPosition = newPosition;
      });
    });
  }

  double _calculateDistance(
      double lat1, double lon1, double lat2, double lon2) {
    const double earthRadius = 6371; // in kilometers

    double dLat = _degreesToRadians(lat2 - lat1);
    double dLon = _degreesToRadians(lon2 - lon1);

    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(_degreesToRadians(lat1)) *
            cos(_degreesToRadians(lat2)) *
            sin(dLon / 2) *
            sin(dLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double distance = earthRadius * c;

    return distance;
  }

  double _degreesToRadians(double degrees) {
    return degrees * (pi / 180);
  }

  void _navigateToDetailsPage(String qrCodeData) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PassegginoDetailsPage(qrCodeData: qrCodeData,user: widget.user,),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
        stream: _userStream,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasError) {
            return const Text('Errore');
          }

          if (snapshot.connectionState != ConnectionState.active ||
              _currentPosition == null) {
            return const SizedBox(); // Return an empty widget if not active or no position
          }

          List<QueryDocumentSnapshot<Map<String, dynamic>>> docs =
              snapshot.data!.docs;
          // Calcola le distanze per tutti gli elementi
          List<Map<String, dynamic>> itemsWithDistance = [];
          for (var item in docs) {
            Map<String, dynamic> data = item.data();

            if (!data.containsKey('inUso') ||
                !data.containsKey('modello') ||
                !data.containsKey('descrizione') ||
                !data.containsKey('prezzo_orario€') ||
                !data.containsKey('latitudine') ||
                !data.containsKey('longitudine') ||
                !data.containsKey('foto')) {
              continue;
            }

            if (data['inUso'] == 'si') {
              // Skip if the item is "in use"
              continue;
            }

            double latitude = data['latitudine'];
            double longitude = data['longitudine'];

            double distance = _calculateDistance(
              _currentPosition!.latitude,
              _currentPosition!.longitude,
              latitude,
              longitude,
            );

            data['distanza'] = distance;
            itemsWithDistance.add(data);
          }

          // Ordina gli elementi in base alla distanza
          itemsWithDistance
              .sort((a, b) => a['distanza'].compareTo(b['distanza']));

          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.all(20),
                  itemCount: itemsWithDistance.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = itemsWithDistance[index];
                    double distance = item['distanza'];

                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.90,
                        height: 210,
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(40),
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    15, 15, 15, 30),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Align(
                                          alignment: AlignmentDirectional(0, 0),
                                          child: Container(
                                            width: 105,
                                            height: 105,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image:
                                                    Image.network(item['foto'])
                                                        .image,
                                              ),
                                              shape: BoxShape.circle,
                                            ),
                                            alignment:
                                                AlignmentDirectional(0, 0),
                                          ),
                                        ),
                                        ElevatedButton(
                                          onPressed: () =>
                                              _navigateToDetailsPage(
                                                  item['qr']),
                                          child: Text(
                                            'Noleggia',
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(50.0),
                                              ),
                                            ),
                                            backgroundColor:
                                                MaterialStateProperty
                                                    .resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                                if (states.contains(
                                                    MaterialState.pressed)) {
                                                  return Colors.white;
                                                }
                                                return Colors.white;
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            10, 0, 0, 0),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.max,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                color: _disponibileColor(
                                                    item['inUso']),
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    width: 100,
                                                    height: 20,
                                                    child: Text(
                                                      item['modello'],
                                                      style:
                                                          GoogleFonts.getFont(
                                                        'Open Sans',
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 17,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: EdgeInsetsDirectional
                                                    .fromSTEB(0, 0, 0, 0),
                                                child: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.max,
                                                  children: [
                                                    Expanded(
                                                      child: ListView(
                                                        padding:
                                                            EdgeInsets.zero,
                                                        scrollDirection:
                                                            Axis.vertical,
                                                        children: [
                                                          Container(
                                                            width: 100,
                                                            height: 100,
                                                            child: Text(
                                                              item[
                                                                  'descrizione'],
                                                              style: GoogleFonts
                                                                  .getFont(
                                                                'Open Sans',
                                                                fontSize: 14,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Text(
                                              '€ ${item['prezzo_orario€']} / h',
                                              style: GoogleFonts.getFont(
                                                'Open Sans',
                                                fontSize: 14,
                                                color: Colors.blueAccent,
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.place_outlined,
                                                  color: Colors.white54,
                                                ),
                                                Text(
                                                  '${distance.toStringAsFixed(2)} km',
                                                  style: GoogleFonts.getFont(
                                                    'Open Sans',
                                                    fontSize: 14,
                                                    color: Colors.white54,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
