import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:math' show asin, atan2, cos, pi, sin, sqrt;

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  late final GoogleMapController _controller;
  bool _isLoading = true;
  bool _hasLocationPermission = false;
  Set<Marker> _markers = {};
  Position? _currentPosition;

  @override
  void initState() {
    super.initState();
    checkLocationPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _buildGoogleMap(),
          if (_isLoading) _buildLoadingIndicator(),
        ],
      ),
    );
  }

  Widget _buildGoogleMap() {
    return _hasLocationPermission
        ? GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: _currentPosition != null
                  ? LatLng(
                      _currentPosition!.latitude, _currentPosition!.longitude)
                  : LatLng(0, 0),
              zoom: 14.0,
            ),
            onMapCreated: (controller) {
              _controller = controller;
              getCurrentLocation(); // Call getCurrentLocation here
              fetchMarkers();
            },
            myLocationEnabled: true,
            markers: _createMarkerSet(),
          )
        : Center(child: Text('Permission denied'));
  }

  Widget _buildLoadingIndicator() {
    return Center(child: CircularProgressIndicator());
  }

  void checkLocationPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      setState(() {
        _isLoading = false;
        _hasLocationPermission = false;
      });
    } else {
      setState(() {
        _isLoading = true;
        _hasLocationPermission = true;
      });

      // Otteniamo la posizione corrente e poi chiamiamo fetchMarkers()
      getCurrentLocation();
    }
  }

  void getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    setState(() {
      _currentPosition = position;
      _isLoading = false;
    });

    // Dopo aver ottenuto la posizione, sposta la mappa sulla posizione corrente
    if (_controller != null && _currentPosition != null) {
      _controller.animateCamera(
        CameraUpdate.newLatLngZoom(
          LatLng(_currentPosition!.latitude, _currentPosition!.longitude),
          14.0,
        ),
      );
    }

    // Ora chiamiamo fetchMarkers() per ottenere i marker filtrati vicino alla posizione corrente
    fetchMarkers();
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
    fetchMarkers();
  }

  Set<Marker> _createMarkerSet() {
    return _markers;
  }

  void fetchMarkers() async {
    QuerySnapshot querySnapshot =
        await FirebaseFirestore.instance.collection('passeggini').get();
    List<Marker> markers = [];
    querySnapshot.docs.forEach((doc) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      double? lat = data['latitudine'];
      double? lon = data['longitudine'];
      String? modello = data['modello'];
      String? descrizione = data['descrizione'];
      String? inUso = data['inUso'];
      double? prezzoOrario = data['prezzo_orario'];
      String? fotoUrl = data['foto'];

      if (lat != null && lon != null && inUso == 'no') {
        // Aggiunto inUso == 'no'
        LatLng markerLocation = LatLng(lat, lon);
        Marker marker = Marker(
          markerId: MarkerId(doc.id),
          position: markerLocation,
          onTap: () {
            _showMarkerDetails(
              modello,
              descrizione,
              inUso,
              prezzoOrario,
              fotoUrl,
              markerLocation,
            );
          },
        );
        markers.add(marker);
      }
    });

    setState(() {
      _markers = markers.toSet();
    });
  }

  void _showMarkerDetails(
    String? modello,
    String? descrizione,
    String? inUso,
    double? prezzoOrario,
    String? fotoUrl,
    LatLng markerLocation,
  ) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        double distance = calculateDistance(
          _currentPosition?.latitude ?? 0.0,
          _currentPosition?.longitude ?? 0.0,
          markerLocation.latitude,
          markerLocation.longitude,
        );
        return AlertDialog(
          backgroundColor: Colors.transparent,
          content: ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.90,
              height: 240,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(15, 15, 15, 30),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  width: 105,
                                  height: 105,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(fotoUrl ?? ''),
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ),
                            ],
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      color: _disponibileColor(inUso),
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          width: 100,
                                          height: 20,
                                          child: Text(
                                            modello ?? '',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.only(top: 10),
                                      child: SingleChildScrollView(
                                        child: Container(
                                          width: 100,
                                          height: 100,
                                          decoration: BoxDecoration(
                                            color: Color(0x00FFFFFF),
                                          ),
                                          child: Align(
                                            alignment: Alignment.center,
                                            child: Text(
                                              descrizione ?? '',
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.grey[900],
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                        bottomRight: Radius.circular(40),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Text(
                            'Distanza:',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: 10),
                          Text(
                            '${distance.toStringAsFixed(2)} km da te',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Color _disponibileColor(String? inUso) {
    if (inUso == 'true') {
      return Colors.red;
    } else {
      return Colors.green;
    }
  }

  double calculateDistance(
    double lat1,
    double lon1,
    double lat2,
    double lon2,
  ) {
    const p = 0.017453292519943295;
    final c = cos;
    final a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
}

void main() {
  runApp(MaterialApp(
    title: 'Map Example',
    home: MapSample(),
  ));
}
