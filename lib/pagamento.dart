import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share/welcome_screen.dart';

class PaymentPage extends StatefulWidget {
  final User user;

  const PaymentPage({Key? key, required this.user}) : super(key: key);
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  TextEditingController rechargeAmountController = TextEditingController();
  late double credit;
  void rechargeCredit(BuildContext context) {
    double amount = double.tryParse(rechargeAmountController.text) ?? 0;
    if (amount > 0) {
      Provider.of<Globals>(context, listen: false).updateCredit(amount,widget.user);
      rechargeAmountController.text = '';
    }
  }

  @override
  void dispose() {
    rechargeAmountController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Dettagli per la ricarica',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Numero della C/C',
              ),
            ),
            SizedBox(height: 16),
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Data di scadenza',
                    ),
                  ),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: 'CVV',
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),
            TextFormField(
              controller: rechargeAmountController,
              decoration: InputDecoration(
                labelText: 'Importo da ricaricare',
              ),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () => rechargeCredit(context),
              child: Icon(Icons.credit_card),
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0))),
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) {
                    if (states.contains(MaterialState.pressed)) {
                      return Colors.deepOrange;
                    }
                    return Colors.black;
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
