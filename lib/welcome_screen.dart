import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'package:share/pagamento.dart';
import 'package:share/profilo.dart';
import 'package:share/qrcode.dart';

import 'home.dart';
import 'mappa.dart';

class Globals extends ChangeNotifier {
  double _credit = 0;
  double get credit => _credit;
  Globals(double credit) : _credit = credit;
  Future<void> getCreditFromFirestore(User user) async {
    try {
      final userRef =
          FirebaseFirestore.instance.collection('users').doc(user.uid);
      final snapshot = await userRef.get();
      final data = snapshot.data();
      if (data != null && data['credit'] != null) {
        _credit = data['credit'];
        notifyListeners();
      }
    } catch (e) {
      print('Errore nel recuperare il credito da Firestore: $e');
    }
  }

  void updateCredit(double amount, User user) {
    _credit += amount;
    _saveCreditToFirestore(_credit, user); // Aggiorna il credito nel database
    notifyListeners(); // Notifica ai widget ascoltatori che il valore è cambiato
  }

  void deductCredit(double amount, User user) {
    _credit -= amount;
    _saveCreditToFirestore(_credit, user); // Aggiorna il credito nel database
    notifyListeners();
  }

  Future<void> _saveCreditToFirestore(double credit, User user) async {
    try {
      final userRef =
          FirebaseFirestore.instance.collection('users').doc(user.uid);

      final creditData = {
        'credit': credit,
      };

      await userRef.set(creditData, SetOptions(merge: true));
      print('Credito aggiornato su Firestore: $credit');

      // Quando il credito viene salvato nel database, aggiorniamo anche il valore in Globals.credit
      _credit = credit;
      notifyListeners();
    } catch (e) {
      print('Errore nell\'aggiornare il credito su Firestore: $e');
    }
  }
}

class HomePage extends StatefulWidget {
  final User user;

  const HomePage({Key? key, required this.user}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int indice = 0;

  List<Widget> schermi = [];

  String? _currentAddress;
  Position? _currentPosition;

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    Provider.of<Globals>(context, listen: false)
        .getCreditFromFirestore(widget.user);

    schermi = [
      Home(
        user: widget.user,
      ),
      MapSample(),
      PaymentPage(user: widget.user),
      QRViewExample(user: widget.user),
    ];
    _getCurrentPosition();
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() => _currentPosition = position);
    }).catchError((e) {
      debugPrint(e);
    });
  }

  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final _userStream =
      FirebaseFirestore.instance.collection('passeggini').snapshots();

  Future<void> _signOut() async {
    await _auth.signOut();
    await _googleSignIn.signOut();
    Navigator.pop(context);
  }

  String _disponibile(String s) {
    if (s == "si")
      return "offline";
    else
      return "online";
  }

  void click(int index) {
    print(index);
    if (index == 1) {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (BuildContext context) => MapSample()));
    } else if (index == 2) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => PaymentPage(user: widget.user)));
    }
  }

  @override
  Widget build(BuildContext context) {
    final credit = Provider.of<Globals>(context).credit;
    String creditoFormattato = credit.toStringAsFixed(2);
    if (schermi.isEmpty) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Loading...'),
        ),
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Scaffold(
      backgroundColor: Color.fromARGB(100, 60, 60, 60),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          'Stroller Sharing',
          style: TextStyle(fontWeight: FontWeight.w900, color: Colors.white),
        ),
        backgroundColor: Colors.black,
        bottom: PreferredSize(
          child: Container(
            color: Colors.black,
            child: Row(children: [
              Text(
                "Saldo: $creditoFormattato €",
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 19,
                  color: Colors.lightBlueAccent,
                ),
              )
            ]),
          ),
          preferredSize: Size.fromHeight(10),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.person,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => Profilo(
                user: widget.user,
              ),
            )),
          ),
        ],
      ),
      body: schermi[indice],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (value) => setState(() => indice = value),
        currentIndex: indice,
        selectedItemColor: Colors.deepOrange,
        showSelectedLabels: false,
        backgroundColor: Colors.black,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.stroller, color: Colors.white),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map, color: Colors.white),
            label: 'map',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.credit_card, color: Colors.white),
            label: '+',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.photo_camera, color: Colors.white),
            label: 'foto',
          ),
        ],
      ),
    );
  }
}
