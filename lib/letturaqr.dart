import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:share/welcome_screen.dart';
import 'package:intl/intl.dart';

class PassegginoDetailsPage extends StatefulWidget {
  final String qrCodeData;
  final User user;

  const PassegginoDetailsPage(
      {Key? key, required this.qrCodeData, required this.user})
      : super(key: key);

  @override
  _PassegginoDetailsPageState createState() => _PassegginoDetailsPageState();
}

class _PassegginoDetailsPageState extends State<PassegginoDetailsPage> {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  double prezzoOrario = 0.0;
  double prezzoFisso = 0.5;
  double price = 0;
  late Completer<void> _dialogCompleter;

  late GlobalKey<_CronoState> cronoKey;
  late Globals globalsProvider;

  @override
  void initState() {
    super.initState();
    cronoKey = GlobalKey<_CronoState>();
    globalsProvider = Provider.of<Globals>(context, listen: false);
    _dialogCompleter = Completer<void>();
    _fetchData();
  }

  Future<void> _fetchData() async {
    // Recupera il prezzo orario dal database utilizzando 'qrCodeData'
    final snapshot = await FirebaseFirestore.instance
        .collection('passeggini')
        .where('qr', isEqualTo: widget.qrCodeData)
        .get();

    if (snapshot.docs.isNotEmpty) {
      final passegginoData = snapshot.docs.first.data();
      setState(() {
        prezzoOrario = passegginoData['prezzo_orario€'] ?? 0.0;
        // If prezzo_orario is null in Firebase, set it to 0.0 as a fallback.
      });
    }
  }

  void _saveRentalHistory(int elapsed) async {
    // Ottieni la data corrente
    String currentDate = DateFormat('yyyy-MM-dd HH:mm').format(DateTime.now());

    // Calcola l'importo del noleggio in base al tempo trascorso
    double price = ((elapsed / 60000) * prezzoOrario / 60);
    double totalAmount = price + prezzoFisso;

    // Crea un nuovo documento per lo storico del noleggio
    // Utilizza l'ID dell'utente come nome del documento per identificare lo storico per quell'utente specifico
    await FirebaseFirestore.instance
        .collection(
            'users') // Collection "users" per memorizzare lo storico per ogni utente
        .doc(widget.user.uid) // Document ID associato all'utente corrente
        .collection(
            'rental_history') // Sottocollezione per lo storico di noleggio
        .add({
      'date': currentDate, // Data e ora corrente del noleggio
      'amount': totalAmount, // Importo totale del noleggio
      'duration': elapsed, // Tempo trascorso in millisecondi
    });
  }

  void _updateInUsoField(String value) async {
    print('QR Code Data: ${widget.qrCodeData}');
    final passegginoQuery = await _db
        .collection('passeggini')
        .where('qr', isEqualTo: widget.qrCodeData)
        .get();

    // Controlla se il documento esiste prima di tentare di aggiornarlo

    final passegginoRef = passegginoQuery.docs.first.reference;
    passegginoRef.update({'inUso': value});

    // Il documento non esiste, puoi gestire questo caso come preferisci.
    // Ad esempio, puoi creare il documento o mostrare un messaggio di errore.
  }

  void _updateLocationFields(double latitude, double longitude) async {
    final passegginoQuery = await _db
        .collection('passeggini')
        .where('qr', isEqualTo: widget.qrCodeData)
        .get();

    if (passegginoQuery.docs.isNotEmpty) {
      final passegginoRef = passegginoQuery.docs.first.reference;
      passegginoRef.update({
        'inUso': 'no',
        'latitudine': latitude,
        'longitudine': longitude,
      });
    }
  }

  void _showThankYouDialog(BuildContext context, int elapsed) {
    String seconds = ((elapsed ~/ 1000) % 60).toString().padLeft(2, "0");
    String minutes = ((elapsed ~/ 1000) ~/ 60).toString().padLeft(2, "0");

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        // Round the price and other values
        double roundedPrice = double.parse(price.toStringAsFixed(3));
        double roundedPrezzoOrario =
            double.parse(prezzoOrario.toStringAsFixed(3));
        double roundedPrezzoFisso =
            double.parse(prezzoFisso.toStringAsFixed(3));
        double roundedTotalPrice = double.parse(
            (roundedPrice + roundedPrezzoFisso).toStringAsFixed(3));

        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.90,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Grazie',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Text(
                  'Il pagamento di € $roundedTotalPrice è stato effettuato con successo!',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Prezzo orario:',
                      style: TextStyle(color: Colors.blue, fontSize: 16),
                    ),
                    Text(
                      '€ $roundedPrezzoOrario' '0',
                      style: TextStyle(color: Colors.blue, fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Prezzo fisso:',
                      style: TextStyle(color: Colors.red, fontSize: 16),
                    ),
                    Text(
                      '€ $roundedPrezzoFisso' '0',
                      style: TextStyle(color: Colors.red, fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Prezzo totale:',
                      style: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '€ $roundedTotalPrice',
                      style: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Tempo:',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '$minutes:$seconds',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        );
      },
    );
  }

  void _onCronoStateChanged(bool isRunning, int elapsed) async {
    if (!isRunning) {
      String isCronoRunning = isRunning ? 'si' : 'no';
      _updateInUsoField(isCronoRunning);

      // Ottieni la posizione corrente
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      // Aggiorna i campi nel database con le nuove coordinate
      _updateLocationFields(position.latitude, position.longitude);

      // Calcola il tempo trascorso dal cronometro e il prezzo in base al tempo
// Calcola il tempo trascorso dal cronometro e il prezzo in base al tempo
      int elapsedTime = elapsed; // Prezzo orario del passeggino
      price = ((elapsedTime / 60000) * prezzoOrario / 60);

      double prezzototale = price + prezzoFisso;
      print(
          "$elapsedTime e il prezzo da togliere $prezzototale, e quello senza prezzofisso è $price");
      // Arrotonda il prezzo a due decimali
      price = double.parse(price.toStringAsFixed(2));

      // Sottrai il prezzo dal saldo corrente
      globalsProvider.deductCredit(prezzototale, widget.user);

      // Aspetta un po' di tempo per mostrare la dialog

      // Mostra la dialog di ringraziamento
      _showThankYouDialog(context, elapsed);
      _saveRentalHistory(elapsed);

      // Aspetta ulteriori 4 secondi prima di tornare alla pagina Home
      await Future.delayed(Duration(seconds: 5));

      // Torna alla pagina Home
      Navigator.pop(context);
      await Future.delayed(Duration(seconds: 1));
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    cronoKey = GlobalKey<_CronoState>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
        future: FirebaseFirestore.instance
            .collection('passeggini')
            .where('qr', isEqualTo: widget.qrCodeData)
            .get(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return Center(child: Text('Errore: ${snapshot.error}'));
          }

          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return Center(child: Text('Nessun dato trovato'));
          }

          final passegginoData = snapshot.data!.docs.first.data();
          prezzoOrario = (passegginoData['prezzo_orario€'] ?? 0.0).toDouble();

          // Esegui l'aggiornamento solo se il documento è stato caricato correttamente

          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    '${passegginoData['modello']}',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 16),
                const SizedBox(height: 16),
                Align(
                  alignment: AlignmentDirectional(0, 0),
                  child: Container(
                    width: 300,
                    height: 300,
                    decoration: BoxDecoration(
                      color: Colors.white24,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: Image.network(passegginoData['foto']).image,
                      ),
                      shape: BoxShape.circle,
                    ),
                    alignment: AlignmentDirectional(0, 0),
                  ),
                ),
                const SizedBox(height: 16),
                const SizedBox(height: 16),
                Crono(key: cronoKey, onCronoStateChanged: _onCronoStateChanged),
              ],
            ),
          );
        },
      ),
    );
  }
}

// Resto del codice per il widget Crono come prima

class Crono extends StatefulWidget {
  final Function(bool, int) onCronoStateChanged;
  Crono({required this.onCronoStateChanged, required key});
  @override
  _CronoState createState() => _CronoState();
}

class _CronoState extends State<Crono> {
  late GlobalKey<_CronoState> cronoKey;
  late Stopwatch stopwatch;

  late Timer t;
  int getElapsedTime() {
    return stopwatch.elapsed.inMilliseconds;
  }

  void handleStartStop() {
    setState(() {
      if (stopwatch.isRunning) {
        stopwatch.stop();
      } else {
        stopwatch.start();
      }
      widget.onCronoStateChanged(stopwatch.isRunning, getElapsedTime());
    });
  }

  String returnFormattedText() {
    setState(() {});
    var milli = stopwatch.elapsed.inMilliseconds;

    String milliseconds = (milli % 1000).toString().padLeft(3, "0");
    String seconds = ((milli ~/ 1000) % 60).toString().padLeft(2, "0");
    String minutes = ((milli ~/ 1000) ~/ 60).toString().padLeft(2, "0");
    String hours = ((milli ~/ 1000) ~/ 3600).toString().padLeft(2, "0");

    return "$hours:$minutes:$seconds";
  }

  @override
  void initState() {
    super.initState();
    stopwatch = Stopwatch();
    t = Timer.periodic(Duration(milliseconds: 30), (timer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    stopwatch.stop();
    t.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CupertinoButton(
            onPressed: () {
              handleStartStop();
            },
            padding: EdgeInsets.all(0),
            child: Container(
              height: 250,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Color(0xff0395eb),
                  width: 4,
                ),
              ),
              child: Text(
                returnFormattedText(),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}
